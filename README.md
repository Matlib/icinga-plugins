# Description

This repository contains a number of plugins for the Icinga monitoring (and compatibles). The requirements are make(1) for compiling (although you can
launch the `./build` command directly) and perl(1) version 5.10+.

The following plugins are available:

* *check_memory* – memory and swap usage,

* *check_filesystem* – space and inode usage on chosen filesystems,

* *check_smartctl* – disk's S.M.A.R.T. health status (requires smartctl(8)).

The `etc/` directory contains Icinga's configuration files for each plugin.

# Author

Originally written and maintained by [Matlib](mailto:matlib@matlibhax.com).
