OUTPUT=nagios
PMEM=$(OUTPUT)/check_memory
PFS=$(OUTPUT)/check_filesystem
PSMRT=$(OUTPUT)/check_smartctl

all: $(PMEM) $(PFS) $(PSMRT)

clean:
	rm -rf $(OUTPUT)

help:
	@echo
	@echo "Available targets:"
	@echo "  all"
	@echo "  clean"
	@echo " " $(PMEM)
	@echo " " $(PFS)
	@echo " " $(PSMRT)
	@echo
	@echo "Plugins will be built in" $(OUTPUT)
	@echo "Icinga configuration files are in etc"
	@echo

$(PMEM): Makefile build src/check_memory src/sub/*
	./build $(PMEM)

$(PFS): Makefile build src/check_filesystem src/sub/*
	./build $(PFS)

$(PSMRT): Makefile build src/check_smartctl src/sub/*
	./build $(PSMRT)
