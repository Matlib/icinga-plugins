sub print_value
{
	my ($value, $with_unit, $max) = @_;
	if (defined $options{unit})
	{
		unless ($options{unit_norm})
		{
			$options{unit} = lc ($options{unit});
			die "Invalid unit '$options{unit}' selected.\n"
				 unless grep { $_ eq $options{unit} }
				 ('b', 'k', 'm', 'g', 't', 'kb', 'mb', 'gb', 'tb');
			$options{unit_norm} = substr ($options{unit}, 0, 1);
		}
		$max = 1;
		$max = 1024 if $options{unit_norm} eq 'k';
		$max = 1048576 if $options{unit_norm} eq 'm';
		$max = 1073741824 if $options{unit_norm} eq 'g';
		$max = 1099511627776 if $options{unit_norm} eq 't';
	}
	else
	{
		$max = $value unless defined $max;
	}
	return sprintf ('%.2f%s', $value / 1099511627776, $with_unit ? ' TB' : '')
		 if $max >= 1099511627776;
	return sprintf ('%.2f%s', $value / 1073741824, $with_unit ? ' GB' : '')
		 if $max >= 1073741824;
	return sprintf ('%.2f%s', $value / 1048576, $with_unit ? ' MB' : '')
		 if $max >= 1048576;
	return sprintf ('%.2f%s', $value / 1024, $with_unit ? ' KB': '')
		 if $max >= 1024;
	return $value . (($with_unit and $with_unit > 1) ? ' B' : '');
}
