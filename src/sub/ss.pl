sub ss
{
	my ($string, $offset, $length) = @_;
	return undef unless defined $string;
	return '' if $offset > length ($string);
	return defined $length ? substr ($string, $offset, $length)
		 : substr ($string, $offset);
}
