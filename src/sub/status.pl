sub status
{
	my ($code, $text) = @_;
	my @codes = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN');
	print ("$PLUGIN $codes[$code] - $text\n");
	exit ($code);
}
