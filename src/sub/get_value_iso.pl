sub get_value_iso
{
	local $_;
	my ($value, $unit) = @_;
	return $value if not defined $unit;
	my $m = 1;
	foreach ('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
	{
		return $value * $m if $unit eq $_;
		$m *= 1000;
	}
	my $uunit = uc ($unit);
	return $value * 1024 if $uunit eq 'K' or $uunit eq 'KB' or $unit eq 'KiB'
		 or $unit eq 'Ki';
	$m = 1048576;
	foreach ('M', 'G', 'T', 'P', 'E', 'Z', 'Y')
	{
		return $value * $m if $uunit eq $_ or $unit eq "${_}iB"
			 or $unit eq "${_}i";
		$m *= 1024;
	}
	status (3, "Cannot parse value '$value $unit'");
}
