sub print_time
{
	my $time = shift;
	my $str = '';
	if ($time >= 31536000)
	{
		$str .= ' ' . int ($time / 31536000) . ' y';
		$time %= 31536000;
	}
	if ($time >= 2627424)
	{
		$str .= ' ' . int ($time / 2627424) . ' m';
		$time %= 2627424;
	}
	if ($time >= 86400)
	{
		$str .= ' ' . int ($time / 86400) . ' d';
		$time %= 86400;
	}
	$str .= sprintf (' %d:%02d', int ($time / 3600), int ($time % 3600 / 60))
		 if $time;

	return substr ($str, 1);
}
