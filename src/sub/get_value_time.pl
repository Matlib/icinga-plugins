sub get_value_time
{
	local $_;
	my ($value, $max) = @_;
	die "Invalid value '$value'\n" unless $value =~
		 m/^\s*
		 (?:([0-9,]+|[0-9,]*\.[0-9]+)\s*h)?\s*
		 (?:([0-9,]+|[0-9,]*\.[0-9]+)\s*m)?\s*
		 (?:([0-9,]+|[0-9,]*\.[0-9]+))?\s*
		 (?:([0-9,]+|[0-9,]*\.[0-9]+)\s*%)?\s*
		 $/isx
		 and ((defined $1 or defined $2 or defined $3) xor defined $4);

	my ($h, $m, $s) = ($1 // 0, $2 // 0, $3 // 0);
	$h =~ s/,//g;
	$m =~ s/,//g;
	$s =~ s/,//g;
	if (defined $4)
	{
		die "Percentage not allowed for '$value'\n" unless defined $max;
		$s = $max * $4;
	}

	return 3600 * $h + 60 * $m + $s;
}
