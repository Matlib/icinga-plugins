sub get_value
{
	local $_;
	my ($value, $max) = @_;
	die "Invalid value '$value'\n" unless $value =~
		 m/^\s*(([0-9 ,]+\.?[0-9]*|\.[0-9 ]+)(e-?[0-9]+)?)\s*([a-z]|[a-z]?b|%|e[0-9]+)?\s*$/is;
	$value = $1;
	my $unit = $4;
	$value =~ s/[ ,]//g;
	return $value if not defined $unit;
	return int ($value * $max / 100) if $unit eq '%';
	return $value * 10 ** $1 if ($unit =~ m/^e([0-9]+)/i);
	my $uunit = uc (substr ($unit, 0, 1));
	my $m = 1024;
	foreach ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
	{
		return $value * $m if $uunit eq $_;
		$m *= 1024;
	}
	die "Invalid unit for '$value $unit'\n";
}
