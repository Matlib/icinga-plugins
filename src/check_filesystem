#!/usr/bin/env perl
#
# check_filesystem - monitoring-plugins.org compatible filesystem usage plugin
#
# THIS SOFTWARE IS IN PUBLIC DOMAIN
#   There is no limitation on any kind of usage of this code. 
#   You use it because it is yours.
#
#   Originally written and maintained by Matlib (matlib@matlibhax.com),
#   any comments or suggestions are welcome.
#
# Change log:
#   1  2018-07-31  Matlib
#   2  2018-08-13  Matlib  process df -i output in one run if possible
#

my $PLUGIN = "FS";

use strict;
use warnings;
use utf8;

use POSIX;
use IPC::Open3 qw/open3/;
use Symbol qw/gensym/;
use List::Util qw/max/;
use Getopt::Long qw/:config posix_default gnu_compat bundling/;


my %options;
my @df;
my @fslist;
my ($total, $free);

do
{
	undef $options{glob};
	exit 1 unless GetOptions (\%options, 'warning|w=s', 'critical|c=s',
		 'inode-warning|W=s', 'inode-critical|C=s', 'reserved|R', 'df|d',
		 'glob|g', 'force-mount|m', 'reverse|r', 'unit|u=s', 'version|V', 'help|h');

	if ($options{version})
	{
		print ("check_filesystem version $VERSION\n");
		exit (0);
	}

	if ($options{help})
	{
		print (<<EOT);

check_filesystem v $VERSION

This plugin tests the current filesystem usage.

Usage:
check_filesystem [-d] [-m] [-r] [-R] [-u UNIT]
  -w THRESHOLD -c THRESHOLD -W THRESHOLD -C THRESHOLD [-g] FILESYSTEM ...

Options:
 -h, --help
    Print detailed help screen.
 -V, --version
    Print version information.
 -w, --warning=THRESHOLD
    Exit with WARNING status if filesystem usage is above the warning
    threshold, or when filesystem free space is below this mark.
 -c, --critical=THRESHOLD
    Exit with CRITICAL status if filesystem used or free space exceeds this
    threshold.
 -W, --inode-warning=THRESHOLD
    Exit with WARNING status if inode count is above this warning threshold,
    or when the number of free inodes is below this threshold.
 -C, --inode-critical=THRESHOLD
    Exit with CRITICAL status if inode count exceeds this threshold.
 -d, --df
    Use df(1) to get filesystem counters.
 -g, --glob
    Match against all available filesystems. Must be repeated before every
    pattern.
 -m, --force-mount
    Use mount(8) instead of /proc/mounts or /etc/mtab.
 -R, --reserved
    Count the reserved resources as available.
 -r, --reverse
    Report available values instead of usage. The logic of warning and critical
    marks is reversed too.
 -u, --unit=UNIT
    Report the space values in the given units (k, m and g). Units are chosen
    automatically by default.

The thresholds may be given as percentage of the total (suffixed with '%'),
or as raw bytes, or as number with units.

More detailed information may be accessed with the following command:
  perldoc $0

EOT
		exit (0);
	}

	die "Warning threshold must be specified\n" unless defined $options{warning};
	die "Critical threshold must be specified\n"
		 unless defined $options{critical};
	die "I-node warning threshold must be specified\n"
		 unless defined $options{'inode-warning'};
	die "I-node critical threshold must be specified\n"
		 unless defined $options{'inode-critical'};
	die "Reserved resources are not available for df output\n"
		 if $options{reserved} and $options{df};

	die "Filesystem mount points must be specified.\n" unless @ARGV;
	my $mpt = shift (@ARGV);

	if ($options{glob})
	{
		eval { require File::FnMatch; };
		die "Cannot import File::FnMatch needed for globbing.\n" if $@;
		get_fs_list ();
		my @matching = grep { File::FnMatch::fnmatch ($mpt, $_, 0) } @fslist;
		goto RAW unless @matching;
		push (@df, { mpt => $_, warning => $options{warning},
			 critical => $options{critical}, iwarning => $options{'inode-warning'},
			 icritical => $options{'inode-critical'} }) foreach (@matching);
	}
	else
	{
		RAW:
		push (@df, { mpt => $mpt, warning => $options{warning},
			 critical => $options{critical}, iwarning => $options{'inode-warning'},
			 icritical => $options{'inode-critical'} });
	}
}
while (@ARGV);

if ($options{df})
{
	my ($i, $dfi, $dfo, $dfe, $l, $f_blocks, $f_avail, $f_iused, $f_ifree,
		 $f_max, $has_usage);
	my @tokens;
	$dfe = gensym ();
	open3 ($dfi, $dfo, $dfe, 'df', '-k', '-i', map { $_->{mpt} } @df)
		 or status (3, "Cannot execute df: $!");
	close ($dfi);

	$l = readline ($dfo);
	unless (length ($l // ''))
	{
		undef $/;
		my $error = readline ($dfe);
		status (3, length ($error // '') ? $error :
			 "df did not return any output");
	}
	chomp ($l);
	@tokens = split (/\s+/, $l);
	for ($i = 0; $i < @tokens; $i++)
	{
		$f_blocks = $i if $tokens [$i] =~ /-blocks?$/i;
		$f_avail = $i if $tokens [$i] =~ /^avail/i;
		$f_iused = $i if $tokens [$i] =~ /^iused/i;
		$f_ifree = $i if $tokens [$i] =~ /^ifree/i;
	}
	status (3, "Cannot determine available inodes from df output")
		 unless defined $f_iused and defined $f_ifree;
	$has_usage = defined $f_blocks and defined $f_avail ? 1 : 0;
	$f_max = max ($f_iused, $f_ifree);
	$f_max = max ($f_max, $f_blocks, $f_avail) if $has_usage;

	while ($l = readline ($dfo))
	{
		$l =~ s/[\s\r\n]*$//;
		@tokens = split (/\s+/, $l);
		status (3, "df output too short") if @tokens < $f_max;
		foreach my $d (@df)
		{
			next if length ($l) < length ($d->{mpt}) + 1
				 or substr ($l, length ($l) - length ($d->{mpt}) - 1)
				 ne ' ' . $d->{mpt};
			status (3, "Cannot determine available space from df output")
				 unless $tokens [$f_iused] =~ m/^[0-9]+$/
				 and $tokens [$f_ifree] =~ m/^[0-9]+$/;
			$d->{itotal} = $tokens [$f_iused] + $tokens [$f_ifree];
			$d->{ifree} = $tokens [$f_ifree];
			if ($has_usage)
			{
				status (3, "Cannot determine available space from df output")
					 unless $tokens [$f_blocks] =~ m/^[0-9]+$/
					 and $tokens [$f_avail] =~ m/^[0-9]+$/;
				$d->{total} = $tokens [$f_blocks] * 1024;
				$d->{free} = $tokens [$f_avail] * 1024;
			}
		}
	}
	close ($dfo);
	close ($dfe);

	unless ($has_usage)
	{
		open3 ($dfi, $dfo, $dfe, 'df', '-k', map { $_->{mpt} } @df)
			 or status (3, "Cannot execute df: $!");
		close ($dfi);
		$l = readline ($dfo);
		unless (length ($l // ''))
		{
			undef $/;
			my $error = readline ($dfe);
			status (3, length ($error // '') ? $error :
				 "df did not return any output");
		}
		chomp ($l);
		@tokens = split (/\s+/, $l);
		$f_blocks = $f_avail = undef;
		for ($i = 0; $i < @tokens; $i++)
		{
			$f_blocks = $i if $tokens [$i] =~ /-blocks?$/i;
			$f_avail = $i if $tokens [$i] =~ /^avail/i;
		}
		status (3, "Cannot determine available inodes from df output")
			 unless defined $f_blocks and defined $f_avail;
		$f_max = max ($f_blocks, $f_avail);

		while ($l = readline ($dfo))
		{
			$l =~ s/[\s\r\n]*$//;
			@tokens = split (/\s+/, $l);
			status (3, "df output too short") if @tokens < $f_max;
			foreach my $d (@df)
			{
				next if length ($l) < length ($d->{mpt}) + 1
					 or substr ($l, length ($l) - length ($d->{mpt}) - 1)
					 ne ' ' . $d->{mpt};
				status (3, "Cannot determine available indes from df output")
					 unless $tokens [$f_blocks] =~ m/^[0-9]+$/
					 and $tokens [$f_avail] =~ m/^[0-9]+$/;
				$d->{total} = $tokens [$f_blocks] * 1024;
				$d->{free} = $tokens [$f_avail] * 1024;
			}
		}
		close ($dfo);
	}
}
else
{
	eval { require Filesys::Statvfs; };
	die "Cannot initialize Filesys::Statvfs.\n" if $@;
	my @fs_info;
	my ($bsize, $frsize, $blocks, $bfree, $bavail, $files, $ffree, $favail);
	get_fs_list ();
	foreach my $d (@df)
	{
		unless (grep { $_ eq $d->{mpt} } @fslist)
		{
			$d->{status} = 3;
			$d->{msg} = 'not mounted';
			next;
		}
		@fs_info = Filesys::Statvfs::statvfs ($d->{mpt});
		unless (@fs_info == 10)
		{
			$d->{status} = 3;
			$d->{msg} = 'statvfs failed';
			next;
		}
		($bsize, $frsize, $blocks, $bfree, $bavail, $files, $ffree, $favail)
			 = @fs_info;
		$d->{total} = $blocks * $frsize;
		$d->{free} = ($options{reserved} ? $bfree : $bavail) * $frsize;
		$d->{itotal} = $files;
		$d->{ifree} = $options{reserved} ? $ffree : $favail;
	}
}



my $max_status = 0;
my $output = '';
my $perf_data = '';
my ($w, $c);

foreach (@df)
{
	$_->{mpt} =~ s/[\000-\037'"; ]//gs;
	if (not defined $_->{free} or not defined $_->{total}
		 or not defined $_->{ifree} or not defined $_->{itotal})
	{
		$max_status = 3;
		$output .= ($output ? '; ' : '') . $_->{mpt} . ' not mounted';
	}
	elsif ($_->{free} > $_->{total} or $_->{ifree} > $_->{itotal})
	{
		$max_status = 3;
		$output .= ($output ? '; ' : '') . $_->{mpt} . ' invalid';
	}
	else
	{
		$output .= ($output ? '; ' : '') . $_->{mpt} .
			 ($options{reverse} ? ' free ' : ' used ');
		$perf_data .= ($options{reverse} ? " 'free " : " 'used ") . $_->{mpt} .
			 "'=";
		$w = get_value ($_->{warning}, $_->{total});
		$c = get_value ($_->{critical}, $_->{total});
		if ($options{reverse})
		{
			$output .= print_value ($_->{free}, 0, $_->{total});
			$perf_data .= $_->{free} . "B;$w;$c;0;" . $_->{total};
			$max_status = 1 if $max_status < 1 and $_->{free} < $w;
			$max_status = 2 if $max_status < 2 and $_->{free} < $c;
		}
		else
		{
			my $used = $_->{total} - $_->{free};
			$output .= print_value ($used, 0, $_->{total});
			$perf_data .= "$used;$w;$c;0;" . $_->{total};
			$max_status = 1 if $max_status < 1 and $used > $w;
			$max_status = 2 if $max_status < 2 and $used > $c;
		}
		$output .= ' / ' . print_value ($_->{total}, 1) . ' inodes ';

		$w = get_value ($_->{iwarning}, $_->{itotal});
		$c = get_value ($_->{icritical}, $_->{itotal});
		if ($options{reverse})
		{
			$output .= int ($_->{ifree} / ($_->{itotal} ? $_->{itotal} : 1)
				 * 100) . '%';
			$perf_data .= " 'inodes " . $_->{mpt} . "'=" . $_->{ifree} .
				 ";$w;$c;0;" . $_->{itotal};
			$max_status = 1 if $max_status < 1 and $_->{free} < $w;
			$max_status = 2 if $max_status < 2 and $_->{free} < $c;
		}
		else
		{
			my $used = $_->{itotal} - $_->{ifree};
			$output .= int ($used / ($_->{itotal} ? $_->{itotal} : 1) * 100)
				 . '%';
			$perf_data .= " 'inodes " . $_->{mpt} . "'=$used;$w;$c;0;" .
				 $_->{itotal};
			$max_status = 1 if $max_status < 1 and $used > $w;
			$max_status = 2 if $max_status < 2 and $used > $c;
		}
	}
}



status ($max_status, $output . '|' . ss ($perf_data, 1));



sub get_fs_list
{
	return if @fslist;
	my $f;
	if (not $options{'force-mount'}
		 and (open ($f, '<', '/proc/mounts') or open ($f, '<', '/etc/mtab')))
	{
		while (readline ($f))
		{
			if (m/^([^ ]|\\ )+\s+(([^ ]|\\ )+)\s+/s)
			{
				my $n = $2;
				$n =~ s/\\([0-7]{3})/chr(oct $1)/egs;
				$n =~ s/\\([0-7]{2})/chr(oct $1)/egs;
				$n =~ s/\\([0-7]{1})/chr(oct $1)/egs;
				$n =~ s/\\(.)/$1/gs;
				push (@fslist, $n);
			}
		}
	}
	elsif (open ($f, '-|', 'mount'))
	{
		my @u = uname ();
		my $p = qr/^.*?\s+on\s+(.*?)\s+(type\s|\()/s;
		$p = qr/^(.*?) on /s if $u [0] eq 'SunOS';
		while (readline ($f))
		{
			push (@fslist, $1) if m/$p/;
		}
	}
	else
	{
		die "Cannot obtain the list of mounted filesystems.\n";
	}
	close ($f);
}

__END__

=pod

=encoding utf8

=head1 NAME

check_filesystem - monitoring-plugins.org compatible filesystem usage plugin

=head1 SYNOPSIS

  check_filesystem [-d] [-m] [-r] [-R] [-u UNIT]
    -w THRESHOLD -c THRESHOLD -W THRESHOLD -C THRESHOLD [-g] FILESYSTEM ...

=head1 DESCRIPTION

This plugin checks for available space and inodes on all filesystems specified
in the command line and reports the status back to the monitoring system.

These options apply to the whole test:

=over 4

=item B<--df>, B<-d>

Try to guess the figues from the output of the L<df(1)> command. The
L<statvfs(2)> system call (requires L<Filesys::Statvfs> to be installed)
is used by default.

=item B<--force-mount>, B<-m>

Use L<mount(8)> for listing available filesystems instead of first trying
to read from F</proc/mounts> or F</etc/mtab>.

=item B<--reserved>, B<-R>

Count the reserved resources as I<free>. Many filesystems implement a pool
of resources available for superuser only. Without this flag they count as
used up. This does not work if the results come from parsing the I<df> output
(I<< L<--df|--df, -d> >>).

=item B<--reverse>, B<-r>

Report resources free instead of used. The meaning of the warning and critical
marks is reversed too.

=item B<--unit>=I<unit>, B<-u> I<unit>

Report the space values in the given units (k, m, g and t). By default the unit
is chosen automatically from the maximum value measured.

=item B<--help>, B<-h>

Print detailed help screen.

=item B<--version>, B<-V>

Print version information.

=back

These options may be repeated and applied to each individual mount point:

=over 4

=item B<--warning>=I<threshold>, B<-w> I<threshold>

Sets the warning mark to the given threshold; the threshold value can be
specified as a direct number or fraction of total filesystem size (see below).
The mark indicates the low or high level depending on what is reported.

=item B<--critical>=I<threshold>, B<-c> I<threshold>

Sets the critical mark for filesystem space.

=item B<--inode-warning>=I<threshold>, B<-W> I<threshold>

Sets the warning mark for free or used inode count. May be specified as a
number or fraction of all allocable inodes.

=item B<--inode-critical>=I<threshold>, B<-C> I<threshold>

Sets the critical mark for inode count.

=item B<--glob>, B<-g>

Make use of globbing characters to match against all available filesystems.
This option is reset every time and must be repeated before every pattern. The
match does not count backslashes like shell globbing does; i.e. specifying
C</dev*> will actually get F</dev> as well as all possible filesystems under
it. The availability of this option depends on L<File::FnMatch>.

=back

The thresholds may be specified as the percentage of the total (suffixed with
C<'%'>), as raw bytes, or as numbers with units. Units recognized are kilobytes
(I<k> or I<kb>), megabytes (I<m> or I<mb>), or gigabytes (I<g> or I<gb>). All
numbers accept fractions and exponential notation (i.e. C<500e6> indicates
500 milion bytes). Spacing is ignored.

Examples:

=over 4

=item

C<50%>

=item

C<.5g> = C<512m> = C<512 m> = C<512 MB> = C<512mb>

=item

C<500e6> = C<500,000,000> = C<5e8> = C<.5e9>

=back

=head1 AUTHOR

=over 4

=item

Matt Latusek L<matlib@matlibhax.com|mailto:matlib@matlibhax.com>

=back

=cut
